## Overview

This is a project that automates the deployment of Docker containers on Amazon EC2 instances using Ansible. The project uses Terraform to provision the EC2 instances and Ansible to configure them and deploy the Docker containers.

### Project Structure
The project has the following directory structure:

terraform-project: This directory contains the Terraform configuration files for provisioning the EC2 instances.
ansible-project: This directory contains the Ansible configuration files for configuring the EC2 instances and deploying the Docker containers.
Jenkinsfile: This file contains the Jenkins pipeline script for executing the Terraform and Ansible commands.
